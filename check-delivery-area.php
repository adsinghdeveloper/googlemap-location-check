<?php

include("decodePolylineToArray.php");

$path = "yhs{DkhanMgb@~B{`@~Jg^lRkZfYoUz^uOlc@iInf@mBbh@lB`h@hInf@tOlc@nU|^jZdYf^lRz`@~Jfb@`Cfb@aC|`@_Kf^mRjZeYlU}^vOmc@fIof@nBah@oBch@gIof@wOmc@mU{^kZgYg^mR}`@_Kgb@_C";


function latLngWithinPoly($latLng, $poly){

  $polySides = count($poly);
  $polyX = array();
  $polyY = array();
  foreach ($poly as $point){
    $polyX[] = $point[0];
    $polyY[] = $point[1];
  }

  $x = $latLng[0];
  $y = $latLng[1];
  return pointInPolygon($polySides, $polyX, $polyY, $x, $y);

}

function decodePolyLine($encoded){
  // decode my custom escaping of / and \ characters....
  $encoded =  str_replace(array(',','-'), array('\\','/'), $encoded);

  $points = decodePolylineToArray($encoded);

  return $points;
}

function latLngWithinMapAreas($latLng, $areas){
  // first, convert the map from json
  $points = decodePolyLine($areas);
  
  $found = latLngWithinPoly($latLng, $points);
  if ($found) {
    return true;
  }else{
    return false;
  } 
}

//$path = "{j{{D_xsmM{|A{`IduDijBvcHycB~a@fpKklAlxI";
//$latLng = array("30.917144656610947","75.82005055541993");
$latLng = explode(',', $_REQUEST["latlng"]);
$paths = json_decode($_REQUEST['area'],true);
//$paths = explode(",", $path);

$response = array("found" => "no");

$deliveryAreas = array();
$deliveryCharges = array();

foreach ($paths as $path) {
  if(latLngWithinMapAreas($latLng, $path["area"])){
    //$deliveryAreas[] = $path;
    $deliveryCharges[] = $path["charges"];
  }
}

if(count($deliveryCharges) > 0){
  $response["found"] = "yes";
  $response["charges"] = min($deliveryCharges);
  //$response["areas"] = $deliveryAreas;
}

header('Content-type: application/json');
echo json_encode($response);

?>